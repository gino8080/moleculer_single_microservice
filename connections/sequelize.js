'use strict';
var chalk = require('chalk'); //color console
var Sequelize = require('sequelize');
var _ = require('lodash');

var db = null;
var sequelize;
const Op = Sequelize.Op;

var ENV = process.env.NODE_ENV || 'development';
// var config = require('../../config/config_' + ENV + '.json')['connections']['postgres']['auth'];

var { createModels } = require('./sequelite_models');

// const initDB = async function (configToUse) {
const initDB = async function () {
    if (db)
        return db;
    db = {};
    var configToUse = require('../config/config_' + ENV + '.json')['connections']['postgres'];
    var defaultConnectOptions = {
        // host: configToUse.auth.host,
        // port: configToUse.auth.port,
        // database: configToUse.auth.database,
        // username: configToUse.auth.username,
        // password: configToUse.auth.password,
        operatorsAliases: Op,
        dialect: 'postgres',
        logging: false,

        define: {
            underscored: false,
            freezeTableName: false,
            charset: 'utf8',
            dialectOptions: {
                collate: 'utf8_general_ci'
            }
            //timestamps: true
        },

        // similar for sync: you can define this to always force sync for models
        sync: {
            //force: true
        },

        // pool configuration used to pool database connections
        pool: {
            max: 2,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    };

    /*    
        // Distruggo e ricreo il db common
        defaultConnectOptions.database = 'postgres';
        sequelize = new Sequelize('postgres', configToUse.common.username, configToUse.common.password, defaultConnectOptions);
        await sequelize.query(`DROP DATABASE IF EXISTS "${configToUse.common.database}"`).then(() => console.log(`POSTGRES Database "${configToUse.common.database}" distrutto!`));
        await sequelize.query(`CREATE DATABASE "${configToUse.common.database}"`).then(() => console.log(`POSTGRES Database "${configToUse.common.database}" creato!`));
    */
    // Inizializzo e memorizzo la connessione al db common
    defaultConnectOptions = await _.merge(defaultConnectOptions, configToUse.common);
    sequelize = new Sequelize(defaultConnectOptions.database, defaultConnectOptions.username, defaultConnectOptions.password, defaultConnectOptions);
    await sequelize
        .authenticate()
        .then(() => {
            console.log(chalk.blue(`POSTGRES Database "${defaultConnectOptions.database}" connesso!`));
            return db;
        })
        .catch(err => {
            console.error(chalk.red(`POSTGRES Database "${defaultConnectOptions.database}" CONNECTION ERROR!`), defaultConnectOptions, err);
        });
    db.common = { sequelize, Sequelize };

    // collego i models e li creo nei db
    db = await createModels(db);

    await db.common
        .sequelize
        .sync({ force: false }) //sincronizzo il db, con force lo butto giù e lo ricreo per adesso
        .then(async function () {
            console.error(chalk.cyanBright(`SYNC ${configToUse.common.database} OK!`));
            return db;
        })
        .catch(error => {
            console.error(chalk.red(`ERRORE NEL SYNC ${configToUse.common.database}`), error);
        });

    return db;
};

module.exports = {
    initDB,
    db
};