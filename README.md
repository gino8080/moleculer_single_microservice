
[![Moleculer](https://img.shields.io/badge/Powered%20by-Moleculer-green.svg?colorB=0e83cd)](https://moleculer.services)

# Microservizio Common di base indipendente
Funziona da solo si appoggia a Moleculer Web che fa da server
e permette anche di chiamare i microservizi tramite route


# moleculer-demo

## Build Setup

``` bash
# Install dependencies
npm install

# Fa aprtire il microservizio indipendente
npm start

# Start developing with REPL parte usando moleculer-runner
npm run dev

# Run unit tests
npm test

# Run continuous test mode
npm run ci

# Run ESLint
npm run lint
```

## Run in Docker

```bash
$ docker-compose up -d --build
```
