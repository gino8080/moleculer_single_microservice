/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {

    /**
     * Carica i models
    */
    const Categories = require('./common/categories')(sequelize, DataTypes);
    const Sections = require('./common/sections')(sequelize, DataTypes);
    const Transcodes = require('./common/transcodes')(sequelize, DataTypes);
    const Languages = require('./common/languages')(sequelize, DataTypes);
    const Drugs = require('./common/drugs')(sequelize, DataTypes);
    const Exams = require('./common/exams')(sequelize, DataTypes);
    const Immunizations = require('./common/immunizations')(sequelize, DataTypes);
    const ICD9CM_D = require('./common/icd9cm_d')(sequelize, DataTypes);
    const ICD9CM_P = require('./common/icd9cm_p')(sequelize, DataTypes);
    const LOINC_IT = require('./common/loinc_it')(sequelize, DataTypes);
    const LOINC_EN = require('./common/loinc_en')(sequelize, DataTypes);



    /**
     * RELAZIONI
     */
    // Codes.hasMany(Transcodes, { foreignKey: 'id0', onDelete: 'CASCADE' });
    // Codes.hasMany(Transcodes, { foreignKey: 'id1', onDelete: 'CASCADE' });


    return { Categories, Sections, Transcodes, Languages, Drugs, Exams, Immunizations, ICD9CM_D, ICD9CM_P, LOINC_IT, LOINC_EN }

};