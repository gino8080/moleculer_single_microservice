module.exports = function (sequelize, DataTypes) {
    /**
     * CATEGORIES
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const Categories = sequelize.define('categories', {
        /**
         * composizione: aaaaa.#######, dove la prima parte a lunghezza fissa 5 caratteri corrisponde al sistema di riferimento con valori: 00000 (interno) | _icd9 | loinc | __cvx | ...
         * seguito da varie sequenze a seconda del sistema di riferimento: per esempio per le codifiche interne la sequenza è 00000.###.###.###, dove
         * la seconda parte è il codice della class, la terza il codice della subclass e l'ultima una sequenza numerica progressiva
         **/
        id: {
            type: DataTypes.STRING(20),
            allowNull: false,
            primaryKey: true
        },
        // sezioni in cui sono usate le funzioni
        sections: {
            type: DataTypes.ARRAY(DataTypes.TEXT),
            allowNull: true,
        },
        // sezione o tipo documento o classificazione di voci: adt | anamnesi | esame obiettivo | diagnosi | ...
        category: {
            type: DataTypes.STRING(20),
            allowNull: true,
        },
        // sottosezione in genere usata per le select specifiche
        subcategory: {
            type: DataTypes.STRING(30),
            allowNull: true
        },
        name_it: {
            type: DataTypes.STRING(150),
            allowNull: true
        },
        name_en: {
            type: DataTypes.STRING(150),
            allowNull: true
        },
        name: {
            type: DataTypes.VIRTUAL(DataTypes.STRING(150), sequelize.literal('name_it AS name'))
            /*              // NON FUNZIONA
                            type: DataTypes.STRING(150),
                                allowNull: true,
                                get() {
                                    return this.getDataValue('name_it');
                                }, */
        },
        // eventuale sistema di codifica con valori: null (default) | icd9 | loinc | cvx | iso639
        codename: {
            type: DataTypes.STRING(10),
            allowNull: true,
            defaultValue: null
        },
        details: {
            type: DataTypes.JSONB,
            allowNull: true
        },
        // eventuale colonna per ordinamento voci personalizzato
        sort: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: null,
        },
        created: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: DataTypes.NOW
        },
        updated: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: DataTypes.NOW
        }
    }, {
            /*  // NON FUNZIONA
                        getterMethods: {
                            name() {
                                return this.name_it;
                            }
                        }, */
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            hooks: {
                beforeUpdate: function (model) {
                    model.updated = sequelize.fn('NOW');
                }
            },
            tableName: 'categories',
            schema: 'common'
        });

    return Categories;
};