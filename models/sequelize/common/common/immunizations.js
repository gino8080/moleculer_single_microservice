module.exports = function (sequelize, DataTypes) {
    /**
     * Immunizations (Vaccini)
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const Immunizations = sequelize.define('immunizations', {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name_it: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        name_en: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'immunizations',
            schema: 'common'
        });

    return Immunizations;
};