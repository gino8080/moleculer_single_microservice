module.exports = function (sequelize, DataTypes) {

    /**
     * TRASCODES
     */
    const Transcodes = sequelize.define('transcodes', {
        id0: {
            type: DataTypes.STRING(20),
            allowNull: false,
            primaryKey: true,
        },
        id1: {
            type: DataTypes.STRING(20),
            allowNull: false,
            primaryKey: true,
        }
    }, {
            tableName: 'transcodes',
            schema: 'common',
            timestamps: false,
            comment: 'tabella di transcodifica da un sistema a un altro'
        });

    return Transcodes;
};