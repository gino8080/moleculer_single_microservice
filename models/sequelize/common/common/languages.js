module.exports = function (sequelize, DataTypes) {
    /**
     * LANGUAGES (ISO 639)
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const Languages = sequelize.define('languages', {
        /**
         * composizione: aaaaa.#######, dove la prima parte a lunghezza fissa 5 caratteri corrisponde al sistema di riferimento con valori: 00000 (interno) | _icd9 | loinc | __cvx | ...
         * seguito da varie sequenze a seconda del sistema di riferimento: per esempio per le codifiche interne la sequenza è 00000.###.###.###, dove
         * la seconda parte è il codice della class, la terza il codice della subclass e l'ultima una sequenza numerica progressiva
         **/
        id: {
            type: DataTypes.STRING(3),
            allowNull: false,
            primaryKey: true
        },
        name_it: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        name_en: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        code_1: {
            type: DataTypes.STRING(2),
            allowNull: true
        },
        code_2t: {
            type: DataTypes.STRING(3),
            allowNull: true
        },
        code_2b: {
            type: DataTypes.STRING(3),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'languages',
            schema: 'common'
        });

    return Languages;
};