module.exports = function (sequelize, DataTypes) {
    /**
     * LOINC EN
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const LOINC_EN = sequelize.define('loinc_en', {
        id: {
            type: DataTypes.STRING(10),
            allowNull: false,
            primaryKey: true
        },
        component: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        property: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        time_aspct: {
            type: DataTypes.STRING(15),
            allowNull: true
        },
        system: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        scale_typ: {
            type: DataTypes.STRING(30),
            allowNull: true
        },
        method_typ: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        class: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        definitiondescription: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        status: {
            type: DataTypes.STRING(11),
            allowNull: true
        },
        classtype: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        unitsrequired: {
            type: DataTypes.STRING(1),
            allowNull: true
        },
        example_units: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'loinc_en',
            schema: 'common'
        });

    return LOINC_EN;
};