module.exports = function (sequelize, DataTypes) {
    /**
     * ICD9CM Procedure
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const ICD9CM_P = sequelize.define('icd9cm_p', {
        id: {
            type: DataTypes.STRING(5),
            allowNull: false,
            primaryKey: true
        },
        description_it: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        description_short_it: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        description_en: {
            type: DataTypes.STRING(200),
            allowNull: true
        },
        description_short_en: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'icd9cm_p',
            schema: 'common'
        });

    return ICD9CM_P;
};