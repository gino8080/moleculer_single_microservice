module.exports = function (sequelize, DataTypes) {
    /**
     * ICD9CM Diagnosi
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const ICD9CM_D = sequelize.define('icd9cm_d', {
        id: {
            type: DataTypes.STRING(5),
            allowNull: false,
            primaryKey: true
        },
        description_it: {
            type: DataTypes.STRING(1000),
            allowNull: true,
        },
        description_short_it: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        description_en: {
            type: DataTypes.STRING(1000),
            allowNull: true
        },
        description_short_en: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'icd9cm_d',
            schema: 'common'
        });

    return ICD9CM_D;
};