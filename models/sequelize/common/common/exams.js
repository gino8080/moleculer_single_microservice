module.exports = function (sequelize, DataTypes) {
    /**
     * EXAMS (Catalogo degli esami di laboratorio e strumentali disponibili)
     * 
     * I dati sono aggiornati al 2015 e scaricati dal Nomenclatore della Regione Lazio: 
     * http://www.regione.lazio.it/binary/rl_sistemi_informativi_sanitari/tbl_contenuti/Nomenclatore_2015_DCA_604_2015.xls
     * 
     * TODO: Aggiungere l'importazione massiva da csv
     */
    const Exams = sequelize.define('exams', {
        id: {
            type: DataTypes.STRING(8),
            allowNull: false,
            primaryKey: true
        },
        type: {
            type: DataTypes.STRING(1),
            allowNull: false
        },
        description_it: {
            type: DataTypes.STRING(750),
            allowNull: false,
        },
        description: {
            type: DataTypes.VIRTUAL(DataTypes.STRING(750), sequelize.literal('description_it AS description'))
        },
        price: {
            type: DataTypes.DECIMAL(12, 2),
            allowNull: true
        },
        branches_codes: {
            type: DataTypes.ARRAY(DataTypes.TEXT),
            allowNull: true,
        },
        branches: {
            type: DataTypes.ARRAY(DataTypes.JSONB),
            allowNull: true,
        },
        note: {
            type: DataTypes.STRING(5),
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'exams',
            schema: 'common'
        });

    return Exams;
};