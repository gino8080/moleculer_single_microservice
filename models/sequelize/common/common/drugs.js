module.exports = function (sequelize, DataTypes) {
    /**
     * Drugs (Farmaci)
     * 
     * L'id corrisponde al codice AIC. La fonte dei dati è l'OpenData di AIFA: http://www.aifa.gov.it/content/dati-sulle-liste-dei-farmaci-open-data
     * La class corrisponde alla classe del farmaco secondo la seguente tabella:
     * - A: comprende i farmaci essenziali e per le malattie croniche il cui costo è a carico dello Stato
     * - C: tutti gli altri farmaci non appartenenti alla fascia "A". La spesa per i farmaci di fascia C è a totale carico del cittadino.
     * - H: comprende i farmaci che, per caratteristiche farmacologiche, per modalità di somministrazione, per innovatività o per altri motivi di salute pubblica sono dispensabili negli ospedali (o per conto di questi nelle farmacie) o somministrabili negli ambulatori specialistici.
     * - V: classe di comodo creata per i vaccini
     */
    const Drugs = sequelize.define('drugs', {
        // Corrisponde al codice AIC
        id: {
            type: DataTypes.STRING(10),
            allowNull: false,
            primaryKey: true,
        },
        name_package: {
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        name: {
            type: DataTypes.VIRTUAL,
            get() {
                const value = this.getDataValue('name_package');
                return value.split('*').shift();
            }
        },
        package: {
            type: DataTypes.VIRTUAL,
            get() {
                const value = this.getDataValue('name_package');
                return value.indexOf('*') > -1 ? value.split('*').pop() : '';
            }
        },
        substances: {
            type: DataTypes.ARRAY(DataTypes.TEXT),
            allowNull: true
        },
        class: {
            type: DataTypes.ENUM('A', 'C', 'H', 'V'),
            allowNull: true
        },
        company: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        group_code: {
            type: DataTypes.STRING(3),
            allowNull: true
        },
        group_description: {
            type: DataTypes.STRING(150),
            allowNull: true
        },
        price: {
            type: DataTypes.DECIMAL(12, 2),
            allowNull: true
        },
        details: {
            type: DataTypes.JSONB,
            allowNull: true
        },
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'drugs',
            schema: 'common',
            comment: "L'id corrisponde al codice AIC. La fonte dei dati è l'OpenData di AIFA: http://www.aifa.gov.it/content/dati-sulle-liste-dei-farmaci-open-data"
        });

    return Drugs;
};