module.exports = function (sequelize, DataTypes) {
    /**
     * DISTRICTS
     */
    const Districts = sequelize.define('districts', {
        // codice/sigla provincia
        id: {
            type: DataTypes.STRING(2),
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        name_it: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        name_en: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        // codice regione
        state_id: {
            type: DataTypes.STRING(3),
            allowNull: true
        },
    }, {
            timestamps: false,
            tableName: 'districts',
            schema: 'location'
        });

    return Districts;
};