module.exports = function (sequelize, DataTypes) {
    /**
     * CITIES
     */
    const Cities = sequelize.define('cities', {
        // codice catastale belfiore
        id: {
            type: DataTypes.STRING(4),
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(35),
            allowNull: false,
            defaultValue: ''
        },
        name_it: {
            type: DataTypes.STRING(35),
            allowNull: false,
            defaultValue: ''
        },
        name_en: {
            type: DataTypes.STRING(35),
            allowNull: false,
            defaultValue: ''
        },
        // codice/sigla provincia
        district_id: {
            type: DataTypes.STRING(2),
            allowNull: true,
        },
        // codice regione
        state_id: {
            type: DataTypes.STRING(3),
            allowNull: true
        },
        code_istat: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        code_area: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        zip: {
            type: DataTypes.STRING(5),
            allowNull: true,
            defaultValue: null,
        },
        residents: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        link: {
            type: DataTypes.STRING(38),
            allowNull: true,
        }
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'cities',
            schema: 'location'
        });

    return Cities;
};