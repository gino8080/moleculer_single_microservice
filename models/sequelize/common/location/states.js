module.exports = function (sequelize, DataTypes) {
    /**
     * STATES
     */
    const States = sequelize.define('states', {
        // codice catastale belfiore
        id: {
            type: DataTypes.STRING(3),
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        name_it: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        name_en: {
            type: DataTypes.STRING(50),
            allowNull: false,
            defaultValue: ''
        },
        old_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
        }
    }, {
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'states',
            schema: 'location'
        });

    return States;
};