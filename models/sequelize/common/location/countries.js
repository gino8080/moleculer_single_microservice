module.exports = function (sequelize, DataTypes) {
    /**
     * COUNTRIES
     */
    const Countries = sequelize.define('countries', {
        // codice ISO 3166-1 Alpha-2
        id: {
            type: DataTypes.STRING(2),
            allowNull: false,
            primaryKey: true
        },
        // codice ISO 3166-1 Alpha-3
        code_a3: {
            type: DataTypes.STRING(3),
            allowNull: false,
            defaultValue: ''
        },
        code_num: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: null
        },
        name_en: {
            type: DataTypes.STRING(70),
            allowNull: false,
            defaultValue: ''
        },
        name_it: {
            type: DataTypes.STRING(70),
            allowNull: false,
            defaultValue: ''
        },
        // FIXME: il campo virtuale restituisce sempre null
        name: {
            type: DataTypes.STRING(70),
            allowNull: true,
            get() {
                return this.getDataValue('name_it');
            },
        },
        link_wikipedia: {
            type: DataTypes.TEXT,
            allowNull: true,
            defaultValue: null
        },
        independent: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        code_old: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        // codice regione
        continent_code: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        area_code: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
    }, {
            // getterMethods: {
            //     name() {
            //         return this.name_it;
            //     }
            // },
            // Aggiunge l'evento beforeUpdate per aggiornare il campo updated
            timestamps: false,
            tableName: 'countries',
            schema: 'location'
        });

    return Countries;
};