/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {

    /**
     * Carica i models
    */
    const Cities = require('./location/cities')(sequelize, DataTypes);
    const Countries = require('./location/countries')(sequelize, DataTypes);
    const Districts = require('./location/districts')(sequelize, DataTypes);
    const States = require('./location/states')(sequelize, DataTypes);


    /**
     * RELAZIONI
     */
    States.hasMany(Districts, { foreignKey: 'state_id', targetKey: 'id' });
    States.hasMany(Cities, { foreignKey: 'state_id', targetKey: 'id' });
    Districts.hasMany(Cities, { foreignKey: 'district_id', targetKey: 'id' });


    return { Cities, Countries, Districts, States }

};