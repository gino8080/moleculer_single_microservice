module.exports = {
    "root": true,
    "env": {
        "node": true,
        "commonjs": true,
        "es6": true,
        "jquery": false,
        "jest": true,
        "jasmine": true
    },
    "extends": "eslint:recommended",
    parser: 'babel-eslint',
    "parserOptions": {
        ecmaVersion: 8,
        sourceType: 'module',
        ecmaFeatures: {
            modules: true,
            async: true,
            await: true,
            spread: true,
            rest: true,
            destructuring: true
            //jsx: true,
        }
    },
    rules: {
        'no-console': 'off',
        "no-unused-vars": [
            1,
            {
                "vars": "all",
                "args": "after-used",
                "ignoreRestSiblings": false
            }
        ]
    }
};