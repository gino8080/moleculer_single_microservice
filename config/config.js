const nconf = require('nconf');
const ENV = nconf.get('NODE_ENV') || 'development';

var configToUse = require(__dirname + '/../config/config_' + ENV + '.json')['connections']['postgres']['auth'];

module.exports = configToUse;