
let BaseValidator = require("../node_modules/moleculer/src/validator");
let { ValidationError } = require("../node_modules/moleculer/src/errors");
let Joi = require('joi');
// --- JOI VALIDATOR CLASS ---
class JoiValidator extends BaseValidator {
  constructor() {
    super();
    this.validator = Joi;
  }

  compile(schema) {
    return (params) => this.validate(params, schema);
  }

  validate(params, schema) {
    const res = this.validator.validate(params, schema);
    if (res.error)
      throw new ValidationError(res.error.message, null, res.error.details);

    return true;
  }
}

module.exports = JoiValidator;