var initDB = require('../connections/sequelize').initDB;
var Joi = require('joi');
var _ = require('lodash');
var Sequelize = require("sequelize");
const Op = Sequelize.Op;
const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {
  name: "common2",
  //version: 1, //broker.call("v1.persons.getAll");
  actions: {


		/**
		 * Say a 'Hello'
		 *
		 * @returns
		 */
    hello() {
      return "COMMON";
    },

    sections: {
      params: Joi.object().keys({
        section: Joi.string()
      }),
      handler(ctx) {
        let qry = {
          // attributes: ['id', , 'name', 'subcategory', 'category'],
          where: { subcategory: null },
          raw: true,
          order: [['sections', 'ASC'], ['category', 'ASC']]
        }
        // if (!_.isEmpty(ctx.params.section)) qry.where = { sections: [ctx.params.section] };
        if (!_.isEmpty(ctx.params.section)) _.assign(qry.where, { sections: [ctx.params.section] });
        return this.MODELS.Categories.findAll(qry).then(res => {
          return _.groupBy(res, 'sections');
          // return _.groupBy(res, 'category');
          // _.forEach(res, function (value, key) {
          //     res[key] = _.groupBy(res[key], function (item) {
          //         return item;
          //     });
          // });
        }).catch(err => err);
      }

    },

    categories: {
      params: Joi.object().keys({
        category: Joi.string().required(),
        subcategory: Joi.string()
      }),
      handler(ctx) {

        return this
          .MODELS
          .Categories
          .findAll({
            where: ctx.params,
            attributes: [
              'id', 'name', 'subcategory', 'category', 'details'
            ],
            raw: true,
            order: [
              ['sort', 'ASC'],
              ['name_it', 'ASC']
            ]
          })
          .then(categories => {
            if (!ctx.params.subcategory) {
              categories = _.groupBy(categories, 'subcategory');
            }
            return categories;
          }).catch(err => err)
      }

    },

    locations: {
      params: Joi.object().keys({
        type: Joi.string().required(),
        id: Joi.string()
      }),
      handler(ctx) {
        const LocationModel = this.MODELS[capitalizeFirstLetter(ctx.params.type)];
        let qry = {
          raw: true,
          attributes: ['id', 'name', 'name_it'],
          order: [['name', 'ASC']]
        }
        if (ctx.params.id > '') {
          qry.where = { id: ctx.params.id };
          return LocationModel.findOne(qry)
            .then(res => {
              return res;
            }).catch(err => err)
        }
        return LocationModel.findAll(qry)
          .then(res => {
            return res;
          }).catch(err => err)
      }

    },
    searchLocations: {
      params: Joi.object().keys({
        type: Joi.string().required(),
        string: Joi.string().required()
      }),
      handler: async function (ctx) {
        // return this.search(ctx.params);
        const LocationModel = this.MODELS[capitalizeFirstLetter(ctx.params.type)];
        return LocationModel.findAll({
          where: { name: { [Op.iLike]: '%' + ctx.params.string + '%' } },
          attributes: ['id', 'name', 'name_it'],
          order: [['name', 'ASC']],
          limit: 50,
          raw: true
        }).then(res => {
          return res;
        }).catch(err => err)
      }

    }

  },
  //EVENTS
  events: {},

  //LIFECYCLE
  created() {
    console.log("Created service " + this.name);
  },
  async started() {
    console.log("Started service " + this.name);
    const MODELS = await initDB();
    this.MODELS = MODELS.common;
  },
  async stopped() {
    console.log("Stopped service " + this.name);
  }
}

