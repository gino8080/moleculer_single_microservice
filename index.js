let path = require("path");
let { ServiceBroker } = require("moleculer");

let JoiValidator = require('./helpers/joiValidator');
let SafeJSONSerializer = require('./helpers/safe-json-serializer');

let broker = new ServiceBroker({
  logger: console,
  namespace: "sanita",
  nodeID: "common2-mservice",
  transporter: "TCP",
  // transporter: "AMQP",
  //serializer: "ProtoBuf",
  requestTimeout: 30000,
  serializer: new SafeJSONSerializer(),
  validation: true,
  validator: new JoiValidator()
});
// Load API Gateway
broker.loadService("./services/api.service");
broker.loadService("./services/common.service");
// Start server
broker.start();